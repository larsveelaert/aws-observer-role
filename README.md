# aws-observer-role

The observerRole is a AWS IAM policy which is more limited than the ReadOnly provided by AWS.

The main purpose is to allow safe exposure of IaC-level of configuration details in an AWS account.

